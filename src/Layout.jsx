import { useState } from "react";
import "./App.scss";
import mockUsers from "./mock-data/data";
import Card from "./components/card";
import Modal from "./components/modal";
import Form from "./components/form";


const Layout = ()=>{

    const [users, setUsers] = useState(mockUsers);
    const [isEditModalOpen, setEditModalOpen] = useState(false);
    const [currentSelectedUser, setCurrentSelectedUser] =useState(null);

    const addToFavourite = (id)=>{
        const temp = users?.map((data)=>{
            if(data.id===id) return {...data, isFavourite: !data.isFavourite}
            return data;
        })
        setUsers(temp);
    }

    const deleteUser = (id)=>{
        const temp = users.filter((data)=> data.id !== id);
        setUsers(temp);
    }


    const editUser = (formData)=>{
         const temp = users?.map((data)=>{
            if(data.id===currentSelectedUser.id) return {...data, ...formData}
            return data;
        })
        setUsers(temp);
        setEditModalOpen(false);
        setCurrentSelectedUser(null);
    }

    const openEditModal = (id)=>{
        const user = users.filter((data)=> data.id===id)[0];
        setCurrentSelectedUser(user);
        setEditModalOpen(true);
    }

    const executeAction = (id, type)=>{
        switch(type){
            case "favourite": addToFavourite(id); break;
            case "delete": deleteUser(id); break;
            case "edit": openEditModal(id); break;
            default: break;
        }
    }

    const closeModal = ()=>{
        setCurrentSelectedUser(null);
        setEditModalOpen(false);
    }

    return (
        <div className="user-data">
            {users.map((data,index)=><Card  {...data} key={data.id} executeAction={executeAction}/>)}

            <Modal isOpen={isEditModalOpen} onClose={closeModal} title="Edit User">
                {isEditModalOpen && <Form onSubmit={editUser} name={currentSelectedUser?.name} email={currentSelectedUser?.email} phone={currentSelectedUser?.phone} website={currentSelectedUser?.website}/>}
            </Modal>
        </div>
    )
}
export default Layout;