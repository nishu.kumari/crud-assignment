import './App.scss';
import Layout from './Layout';

function App() {
  return (
    <div className="container">
     <Layout/>
    </div>
  );
}

export default App;
