const mockUsers = [
  {
    id: 1,
    name: 'John Doe',
    profileImage: 'https://t4.ftcdn.net/jpg/03/64/21/11/360_F_364211147_1qgLVxv1Tcq0Ohz3FawUfrtONzz8nq3e.jpg',
    phone: '555-1234',
    email: 'john.doe@example.com',
    website: "http://hildegard.org",
    isFavourite: false
  },
  {
    id: 2,
    name: 'Jane Smith',
    profileImage: 'https://t4.ftcdn.net/jpg/03/64/21/11/360_F_364211147_1qgLVxv1Tcq0Ohz3FawUfrtONzz8nq3e.jpg',
    phone: '555-5678',
    email: 'jane.smith@example.com',
    website: "http://anastasia.net",
    isFavourite:false
  },
  {
    id: 3,
    name: 'Bob Johnson',
    profileImage: 'https://t4.ftcdn.net/jpg/03/64/21/11/360_F_364211147_1qgLVxv1Tcq0Ohz3FawUfrtONzz8nq3e.jpg',
    phone: '555-9876',
    email: 'bob.johnson@example.com',
    website: "http://ramiro.info",
    isFavourite: false
  },
  {
    id: 4,
    name: 'Emily Davis',
    profileImage: 'https://t4.ftcdn.net/jpg/03/64/21/11/360_F_364211147_1qgLVxv1Tcq0Ohz3FawUfrtONzz8nq3e.jpg',
    phone: '555-4321',
    email: 'emily.davis@example.com',
    website: "http://hildegard.org",
    isFavourite: false
  },
  {
    id: 5,
    name: 'Alex Wilson',
    profileImage: 'https://t4.ftcdn.net/jpg/03/64/21/11/360_F_364211147_1qgLVxv1Tcq0Ohz3FawUfrtONzz8nq3e.jpg',
    phone: '555-8765',
    email: 'alex.wilson@example.com',
    website: "http://hildegard.org",
    isFavourite: false
  },
  {
    id: 6,
    name: 'Sara Brown',
    profileImage: 'https://t4.ftcdn.net/jpg/03/64/21/11/360_F_364211147_1qgLVxv1Tcq0Ohz3FawUfrtONzz8nq3e.jpg',
    phone: '555-2345',
    email: 'sara.brown@example.com',
    website: "http://hildegard.org",
    isFavourite: false
  },
  {
    id: 7,
    name: 'Mike Thompson',
    profileImage: 'https://t4.ftcdn.net/jpg/03/64/21/11/360_F_364211147_1qgLVxv1Tcq0Ohz3FawUfrtONzz8nq3e.jpg',
    phone: '555-6543',
    email: 'mike.thompson@example.com',
    website: "http://hildegard.org",
    isFavourite: false
  },
  {
    id: 8,
    name: 'Anna Garcia',
    profileImage: 'https://t4.ftcdn.net/jpg/03/64/21/11/360_F_364211147_1qgLVxv1Tcq0Ohz3FawUfrtONzz8nq3e.jpg',
    phone: '555-7890',
    email: 'anna.garcia@example.com',
    website: "http://hildegard.org",
    isFavourite: false
  },
  {
    id: 9,
    name: 'Chris Miller',
    profileImage: 'https://t4.ftcdn.net/jpg/03/64/21/11/360_F_364211147_1qgLVxv1Tcq0Ohz3FawUfrtONzz8nq3e.jpg',
    phone: '555-2109',
    email: 'chris.miller@example.com',
    website: "http://hildegard.org",
    isFavourite: false
  },
  {
    id: 10,
    name: 'Megan Turner',
    profileImage: 'https://t4.ftcdn.net/jpg/03/64/21/11/360_F_364211147_1qgLVxv1Tcq0Ohz3FawUfrtONzz8nq3e.jpg',
    phone: '555-1098',
    email: 'megan.turner@example.com',
    website: "http://hildegard.org",
    isFavourite: false
  },
];

export default mockUsers;
