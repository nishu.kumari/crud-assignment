import './index.style.scss'; 

const Modal = ({ isOpen, onClose, children, title }) => {
  const modalVisibility = isOpen ? 'visible' : 'hidden';

  return (
    <div className={`modal-overlay ${modalVisibility}`}>
      <div className="modal-content">
        <div className='modal-header'>
          {title}
          <span className="close-button" onClick={onClose}>&times;</span>
        </div>    
        <div className='modal-body'>{children}</div>
      </div>
     
    </div>
  );
};

export default Modal;
