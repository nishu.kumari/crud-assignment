import "./index.style.scss";
import emailIcon from "../../assets/email.svg";
import phoneIcon from "../../assets/phone.svg";
import websiteIcon from "../../assets/website.svg";
import heartIcon from "../../assets/heart-blank.svg";
import heartFullIcon from "../../assets/heart-full.svg";
import editIcon from "../../assets/edit.svg";
import deleteIcon from "../../assets/delete.svg";

const Card = ({id, name, profileImage,phone, email, website, isFavourite, executeAction})=>{



    return (
        <div className="card">
            <img src={profileImage} alt="profile" className="profile"/>
            <div className="card-details">
                <div className="name">
                    {name}
                </div>
                <div className="text-group">
                    <img src={emailIcon} alt="email"/>
                    <div className="text">{email}</div>
                </div>
                <div className="text-group">
                    <img src={phoneIcon} alt="phone"/>
                    <div className="text">{phone}</div>
                </div>
                <div className="text-group">
                    <img src={websiteIcon} alt="website"/>
                    <div className="text">{website}</div>
                </div>
            </div>
            <div className="card-footer">
                <img src={isFavourite?heartFullIcon:heartIcon} alt="like" onClick={()=> executeAction(id, "favourite")}/>
                <img src={editIcon} alt="edit" onClick={()=> executeAction(id, "edit")}/>
                <img src={deleteIcon} alt="delete" onClick={()=> executeAction(id, "delete")}/>
            </div>
        </div>
    )
}
export default Card;