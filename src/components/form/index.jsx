import React, { useState } from 'react';
import './index.style.scss'; // Import the CSS file for styling

const Form = (props) => {
  const [formData, setFormData] = useState({
    name: props.name,
    email: props.email,
    phone: props.phone,
    website: props.website,
  });

  const [formErrors, setFormErrors] = useState({
    name: '',
    email: '',
    phone: '',
    website: '',
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value,
    });

    setFormErrors({
      ...formErrors,
      [name]: value.trim() === '' ? 'This field is required' : '',
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    const errors = {};
    Object.keys(formData).forEach((key) => {
      if (formData[key].trim() === '') {
        errors[key] = 'This field is required';
      }
    });

    if (Object.keys(errors).length === 0) {
      console.log('Form submitted:', formData);
      props.onSubmit(formData);
    } else {
      setFormErrors(errors);
    }
  };

  return (
    <form onSubmit={handleSubmit} className="custom-form">
      <label>
       <div className='label-text'>Name:</div>
        <input
          type="text"
          name="name"
          value={formData.name}
          onChange={handleChange}
          className={`${formErrors.name && 'error-border'}`}
        />
        {formErrors.name && <span className="error">{formErrors.name}</span>}
      </label>

      <label>
        <div className='label-text'>Email:</div>
        <input
          type="email"
          name="email"
          value={formData.email}
          onChange={handleChange}
          className={`${formErrors.email && 'error-border'}`}
        />
        {formErrors.email && <span className="error">{formErrors.email}</span>}
      </label>

      <label>
        <div className='label-text'>Phone:</div>
        <input
          type="tel"
          name="phone"
          value={formData.phone}
          onChange={handleChange}
          className={`${formErrors.phone && 'error-border'}`}
        />
        {formErrors.phone && <span className="error">{formErrors.phone}</span>}
      </label>

      <label>
        <div className='label-text'>Website:</div>
        <input
          type="url"
          name="website"
          value={formData.website}
          onChange={handleChange}
          className={`${formErrors.website && 'error-border'}`}
        />
        {formErrors.website && <span className="error">{formErrors.website}</span>}
      </label>
      <div className='form-footer'>
        <button >Cancel</button>
        <button type="submit" className='submit'>Edit</button>
      </div>
    </form>
  );
};

export default Form;
